#!/usr/bin/python3
# A simple test for the mqttclient.py
import time
import argparse
import mqttclient

# Mqtt setup
brokerurl = "localhost"
clientid = "mqttclienttester"
pubtopic = "test/yeah"
subtopics = "DMX/state,joypad"
interval = 1.0

# Configure argument parser ..
parser = argparse.ArgumentParser(description="mqttclient.py")
parser.add_argument('--brokerurl', type=str, default=brokerurl, nargs='?', help='The url of the mqtt broker')
parser.add_argument('--clientid', type=str, default=clientid, nargs='?', help='The client id to use for broker connection')
parser.add_argument('--pubtopic', type=str, default=pubtopic, nargs='?', help='The MQTT topic to publish to')
parser.add_argument('--subtopics', type=str, default=subtopics, nargs='?', help='The MQTT topics to subscribe to, comma separated')
parser.add_argument('--interval', type=float, default=interval, nargs='?', help='The interval how quick to post messages')
args = parser.parse_args()

# Define callback for subscriptions
def message_callback(mosq, obj, msg):
    print("Incoming MQTT Message: ", [msg.topic, msg.payload.decode('UTF-8')], flush=True)

# Setup & start mqtt client ..
mqttclient.setup(args.brokerurl, args.clientid, args.subtopics, callback=message_callback)

# Start main loop to publish ..
while True:
    json = '{"menu": {"header": "head", "tester": "buddy", "number": "1"}}'
    mqttclient.publish(args.pubtopic, json)
    time.sleep(args.interval)
