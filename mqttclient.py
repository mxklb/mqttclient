#!/usr/bin/env python
# This is a wrapper around paho.mqtt.client to easily send & receive mqtt messages.
from .logger.logger import logger
import paho.mqtt.client as mqtt
import subprocess as sp
import threading
import logging
import time
import sys

url = None
topics = []
client = None

# Define paho mqtt callback methods ..
def on_log(mosq, obj, level, string):
    logger.debug("LogLevel: " + str(level) + " message = " + string)

def on_subscribe(mosq, obj, mid, granted_qos):
    logger.debug("Subscribed: " + str(mid) + " " + str(granted_qos))

def on_disconnect(client, userdata, rc):
    if rc != 0:
        logger.warning("Unexpected mqtt disconnection")

def on_connect(mosq, obj, rc, other):
    global client, topics, url
    logger.info("Connected to mqtt broker: " + url)
    logger.debug(" - rc: " + str(rc))
    try:
        if len(topics) > 0:
            client.subscribe(topics)
            printTopics = [topic[0] for topic in topics]
            logger.info(" - Subscribed topics: " + str(printTopics))
    except:
        logger.error(" - Failed to subscribe to mqtt broker: " + url)

def on_message(mosq, obj, msg):
    global url
    logger.debug("Message from " + url + " @ topic: " + msg.topic + " -> " + str(msg.payload.decode('UTF-8')))

# Method to connect a client to a mqtt broker ..
def connect_mqtt_broker(client, brokerurl):
    try:
        client.connect(brokerurl)
    except:
        logger.error("Failed to connect to mqtt broker: " + brokerurl)

# Mqtt listener thread ..
def mqtt_listening():
    global client
    try:
        client.loop_forever()
    except: # catch *all* exceptions
        logger.error("Exception: %s" % sys.exc_info()[0])
        sys.exit(1)

# Init and start mqtt subscription lister thread  ..
def start_mqtt_listener_thread():
    mqtt_listener_thread = threading.Thread(target=mqtt_listening)
    mqtt_listener_thread.setDaemon(True)
    mqtt_listener_thread.start()

# Setup topics to subscribe to ..
def init_subscriptions(topicslist):
    global topics
    del topics[:]
    for topic in topicslist.split(','):
        if topic:
            topics.append((topic, 0))
    if len(topics) > 0:
        logger.debug("Setup subscriptions: " + str(topics))

# Check for host ip to be used (only works in docker!)
def get_localhost_ip():
    iproute = sp.check_output(["ip", "route"])
    return iproute.decode().split('\n', 1)[0].split(' ')[2]

## -- Common methods to be used from outside start here -- ##

# Setup paho mqtt client and connect to the mqtt broker ..
def setup(brokerurl, clientid, subscriptions="", callback=on_message):
    global client, url, loglevel
    if brokerurl == "auto":
        brokerurl = get_localhost_ip()
    url = brokerurl
    init_subscriptions(subscriptions)
    client = mqtt.Client(clientid, clean_session=True)
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe
    client.on_message = callback
    client.on_connect = on_connect
    if logger.level == logging.DEBUG:
        client.on_log = on_log
    connect_mqtt_broker(client, url)
    start_mqtt_listener_thread()
    time.sleep(0.5)

# Method to publish json to a topic ..
def publish(topic, json):
    global client
    try: # Publish control data to mqtt broker
        client.publish(topic, json)
        logger.debug("Published to " + url + " @ topic: " + topic + " -> " + str(json))
    except:
        logger.error("Failed to published to " + url + " @ topic: " + topic + " -> " + str(json))
