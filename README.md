# MQTT Client

A simple wrapper around [paho mqtt lib](https://pypi.org/project/paho-mqtt/) to easily post
and subscribe to MQTT topics.

## Setup Dependencies

    pip install paho-mqtt

## How to use this client

    import mqttclient

    # Setup connection details ..
    brokerurl = localhost
    clientid = "testclient"

    # Set topics to subscribe to ..
    subtopics = "topic1/test,topic2/test"

    # Define callback for subscriptions ..
    def message_callback(mosq, obj, msg):
        print("Incoming MQTT Message: ", [msg.topic, msg.payload])

    # Connect and start mqtt listener ..
    mqttclient.setup(brokerurl, clientid, subtopics, callback=message_callback)

    # Example publishing loop ..
    while True:
        json = '{"menu": {"header": "jo", "tester": "olala", "number": "1"}}'
        mqttclient.publish("test/topic", json)
        .. do something else ..

Note: This uses a separate thread for MQTT listening, it uses paho's `client.loop_forever()`.
So disconnections are handled by the paho lib. No need to define any reconnection methods.
Use this abstraction to publish or subscribe to a MQTT broker. When no `subtopics` are
defined (empty string) no subscription is being configured. Provide subscription topics
comma separated. Optionally assign callback to act on incoming messages.

Set environment `LOGLEVEL` to 'INFO', 'WARNING', 'DEBUG' or 'ERROR', default is `LOGLEVEL`='INFO'.

Hint: Just pull this in as a git submodule & install paho-mqtt, so you're ready to go..

## How to test this client

To test `mqttclient` there exists a testing script. Just call

    python mqtttest.py --broker 127.0.0.1 --pubtopic "test/topic" --subtopics "test/topic1,test/topic2"

Make sure to correct the broker url and the publish/subscribe topics according to your needs.
